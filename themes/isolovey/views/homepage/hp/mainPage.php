<?php
/** @var Page $page */

if ($page->layout) {
    $this->layout = "//layouts/{$page->layout}";
}

$this->title = $page->title;
$this->breadcrumbs = [
    $page->title
];
$this->description = !empty($page->description) ? $page->description : Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = !empty($page->keywords) ? $page->keywords : Yii::app()->getModule('yupe')->siteKeyWords
?>

	<div class="row mainPromoWrap">
		<section class="col-sm-4 bigText paddingTop20">
		Объясним суть</br> вашего продукта</br> <strong class = "redColor">за 30 секунд</strong>
		</section>
		<section class="col-sm-4 text-center">
		Пример:</br>
		<iframe src="https://www.youtube.com/embed/Bt9ptuu3u-Y" allowfullscreen="" width="330" height="180" frameborder="0">
		</iframe>
		</section>
	</div>
</div>
<div class="line"></div>
	<div class="container contentWrap">

        <?php 
            $this->widget(
            'bootstrap.widgets.TbBreadcrumbs',
            [
                'links' => $this->breadcrumbs,
                'inactiveLinkTemplate' => ' '
            ]
        );

        $productRepository = Yii::app()->getComponent('productRepository');

        $this->widget(
            'bootstrap.widgets.TbListView',
            [
                'dataProvider' => $productRepository->getListForIndexPage(['spec1', 'spec2', 'spec3', 'spec4', 'spec5']),
                'itemView' => '//store/product/_item',
                'summaryText' => '',
                'enableHistory' => true,
                'cssFile' => false,
                'itemsCssClass' => 'row items',
            ]
        ); ?>

        <?php $this->renderPartial('//store/product/modal');?>
    </div>
