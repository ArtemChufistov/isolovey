<?php  Yii::import('application.modules.store.models.Product');?>
<?php  Yii::import('application.modules.store.models.StoreCategory');?>
<?php $step = 10;?>
<div class="container headButtons">
    <div class="row">
        <div class="col-md-3" style="width: 235px;">
            <button type="button" class="btn btn-default" aria-label="Left Align" id="orderVideo" data-toggle="modal" data-target="#orderVideoModal">Заказать ролик!</button>
        </div>
        <div class="col-md-3" style="width: 250px;">
            <input type="hidden" name="<?= Yii::app()->getRequest()->csrfTokenName;?>" value = "<?= Yii::app()->getRequest()->getCsrfToken();?>">
            <button type="button" class="btn btn-default" aria-label="Left Align" id="calcVideo" data-toggle="modal" data-target="#calcModal">
                Калькулятор стоимости <i class="fa fa-calculator" aria-hidden="true"></i>
            </button>
        </div>
        <div class="col-md-6 marginTop10">
            <strong>mail@isolovey.ru</strong>
            <strong class = "verticalLine">|</strong>
            <span>+7 (383)</span>
            <strong>383-07-16</strong>
        </div>
    </div>
</div>
<div class="line"></div>

<div id="orderVideoModal" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header"><button class="close" type="button" data-dismiss="modal"></button>
            </div>
            <div class="modal-body centeredText marginTop50">
                <h3 class = "">Заказать видеоролик!</h3>
                <p class = "marginTop10"></br>Чтобы заказать видеоролик</br> напишите свой номер сотового телефона</br> и вам перезвонит наш менеджер. 
                </p>
                <form class = "marginTop10" method="post">
                    </br>
                    <input type="text" name="phone" placeholder="+7 (123) 456 78 90" class="orderVIdeoPhone form-control"></br></br>
                    <input type="hidden" name="<?= Yii::app()->getRequest()->csrfTokenName;?>" value = "<?= Yii::app()->getRequest()->getCsrfToken();?>">
                    <button class="btn btn-default redButton marginTop10" type="submit">Заказать ролик!</button></br>
                </form>

            </div>
        </div>
    </div>
</div>

<div id="orderVideoSuccessModal" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header"><button class="close" type="button" data-dismiss="modal"></button>
            </div>
            <div class="modal-body centeredText marginTop50">
                <h3>Ваш запрос отправлен.</h3>
                <p></br>В ближайшее время</br> с Вами свяжется наш менеджер</br> для уточнения деталей заказа. </br>
                </p>
                <i>Спасибо что выбрали нашу видеостудию,</br> надеемся на долгосрочное сотрудничество</i>
            </div>
        </div>
    </div>
</div>

<?php if (!empty($order->id)):?>
    <script type="text/javascript">
        $('#orderVideoSuccessModal').modal();
    </script>
<?php endif;?>

<div id="calcModal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            
            <div class="modal-header"><button class="close" type="button" data-dismiss="modal"></button><h4 class="modal-title">Калькулятор стоимости</h4>
            </div>
            <div class="modal-body">
                <form method="post">
                    <div class="row noPaddingForCols">
                        <section class="col-sm-3 centeredText">
                            </br>
                            <strong style="line-height: 2;">Длительность ролика:</strong>
                        </section>
                        <section class="col-sm-1 centeredText" style = "width: 100px;">
                            <i>Минут</i> 
                            <input type="text" class="form-control bigGreenText" id="minute" name = "minute" value = "1">
                        </section>
                        <section class="col-sm-1 centeredText" style = "width: 100px;">
                            <i>Секунд</i>
                            <input type="text" class="form-control bigGreenText" id="second" name = "second" value = "00">
                        </section>
                        <section class="col-sm-1" style = "width: 11%;">
                            <div class = "plusBtn"></div>
                            <div class = "minusBtn"></div>
                        </section>
                        <section class="col-sm-4" style = "padding-top:15px;">
                            Узнать длительность вашего текста</br>
                            Можно тут: <a target="_blank" style = "color: blue; text-decoration: underline;" href = "http://www.hronomer.ru">www.hronomer.ru</a>
                        </section>
                    </div>
                    <div class="line" style="margin-top: 35px;">
                    </div>
                    <div class="row" style = "text-align: center; margin-top: 30px; margin-bottom: 30px;">
                        <strong class = "centeredText">Уровень сложности</strong>
                    </div>
                    <div class="row levelSlozh">
                        <section class="col-sm-2-5 centeredText" showSpesc = "spec1">
                            <img src = "/img/button1.png"></br>
                            Шаблон</br>
                            (футаж)</br>
                            <i>Когда нужно</br>
                            <strong>очень дешево</strong></br>
                            Но хорошо!</i>
                        </section>
                        <section class="col-sm-2-5 centeredText" showSpesc = "spec2">
                            <img src = "/img/button2.png"></br>
                            Попроще бы</br>
                            <i>Когда <strong>бюджет ограничен,</strong><br> а ролик нужен!</i>
                        </section>
                        <section class="col-sm-2-5 centeredText currentWr" showSpesc = "spec3">
                            <img src = "/img/button3.png"></br>
                            Оптимальный</br>
                            <i>Идеальное соотношение <strong>цена-качество</strong></i>
                        </section>
                        <section class="col-sm-2-5 centeredText" showSpesc = "spec4">
                            <img src = "/img/button4.png"></br>
                            Нам нужен крутой ролик!</br>
                            <i>Детализированный <strong>выделяющийся</strong> видеоролик</i>
                        </section>
                        <section class="col-sm-2-5 centeredText" showSpesc = "spec5">
                            <img src = "/img/button5.png"></br>
                            Имиджевый</br>
                            <i>Покажи всем, что ты <strong>лидер рынка</strong></i>
                        </section>
                    </div>
                    <div class="row specTable spec1">
                        <table>
                            <tr class = "cpecHead">
                                <td colspan="2">Специалисты:</td>
                                <td>Состав работ:</td>
                                <td>Стоимость:</td>
                            </tr>
                            <?php $spec = 'spec1';?>
                            <?php $storeCategory = StoreCategory::model()->findByAttributes(['slug' => $spec]);?>
                            <?php foreach(Product::getByCategorySlug($spec) as $num => $product):?>
                                <tr>
                                    <td>
                                        <img src = "<?php echo $product->getImageUrl();?>">
                                    </td>
                                    <td class ="rightBorderGray">
                                        <strong class = "specName"><?php echo $product->name;?></strong>
                                        <p class = "grayText"><?php echo $product->description;?></p>
                                    </td>
                                    <td class ="rightBorderGray bigRed">
                                        <p class = "bigRed"><?php echo $product->short_description;?></p>
                                    </td>
                                    <td style="width: 140px;">
                                        <strong class = "bigGreen"><?php echo number_format(floor(($product->price / 100) * strip_tags($storeCategory->description)), 0, ',', '');?>
                                        </strong> <span class = "smallGreen">руб.</span>
                                    </td>
                                    <td>
                                    <td>
                                        <input 
                                            type="checkbox"
                                            checked="checked"
                                            cost="<?php echo floor(($product->price / 100) * strip_tags($storeCategory->description));?>"
                                            etalonCost="<?php echo floor(((($product->price / 100) * strip_tags($storeCategory->description))/60)*$step);?>"
                                            name = "product[<?php echo $spec?>][<?php echo $num;?>]"
                                            value = "<?php echo $product->id;?>"
                                            id="checkbox-id-<?php echo $spec;?>-<?php echo $product->id;?>" />
                                        <label for="checkbox-id-<?php echo $spec;?>-<?php echo $product->id;?>"></label>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </table>
                    </div>
                    <div class="row specTable spec2">
                        <table>
                            <tr class = "cpecHead">
                                <td colspan="2">Специалисты:</td>
                                <td>Состав работ:</td>
                                <td>Стоимость:</td>
                            </tr>
                            <?php $spec = 'spec2';?>
                            <?php $storeCategory = StoreCategory::model()->findByAttributes(['slug' => $spec]);?>
                            <?php foreach(Product::getByCategorySlug($spec) as $num => $product):?>
                                <tr>
                                    <td>
                                        <img src = "<?php echo $product->getImageUrl();?>">
                                    </td>
                                    <td class ="rightBorderGray">
                                        <strong class = "specName"><?php echo $product->name;?></strong>
                                        <p class = "grayText"><?php echo $product->description;?></p>
                                    </td>
                                    <td class ="rightBorderGray bigRed">
                                        <p class = "bigRed"><?php echo $product->short_description;?></p>
                                    </td>
                                    <td style="width: 140px;">
                                        <strong class = "bigGreen"><?php echo number_format(floor(($product->price / 100) * strip_tags($storeCategory->description)), 0, ',', '');?>
                                        </strong> <span class = "smallGreen">руб.</span>
                                    </td>
                                    <td>
                                    <td>
                                        <input 
                                            type="checkbox"
                                            checked="checked"
                                            cost="<?php echo floor(($product->price / 100) * strip_tags($storeCategory->description));?>"
                                            etalonCost="<?php echo floor(((($product->price / 100) * strip_tags($storeCategory->description))/60)*$step);?>"
                                            name = "product[<?php echo $spec?>][<?php echo $num;?>]"
                                            value = "<?php echo $product->id;?>"
                                            id="checkbox-id-<?php echo $spec;?>-<?php echo $product->id;?>" />
                                        <label for="checkbox-id-<?php echo $spec;?>-<?php echo $product->id;?>"></label>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </table>
                    </div>
                    <div class="row specTable spec3 currentSpec" style = "display: block;">
                        <table>
                            <tr class = "cpecHead">
                                <td colspan="2">Специалисты:</td>
                                <td>Состав работ:</td>
                                <td>Стоимость:</td>
                            </tr>
                            <?php $spec = 'spec3';?>
                            <?php $storeCategory = StoreCategory::model()->findByAttributes(['slug' => $spec]);?>
                            <?php foreach(Product::getByCategorySlug($spec) as $num => $product):?>
                                <tr>
                                    <td>
                                        <img src = "<?php echo $product->getImageUrl();?>">
                                    </td>
                                    <td class ="rightBorderGray">
                                        <strong class = "specName"><?php echo $product->name;?></strong>
                                        <p class = "grayText"><?php echo $product->description;?></p>
                                    </td>
                                    <td class ="rightBorderGray bigRed">
                                        <p class = "bigRed"><?php echo $product->short_description;?></p>
                                    </td>
                                    <td style="width: 140px;">
                                        <strong class = "bigGreen"><?php echo number_format(floor(($product->price / 100) * strip_tags($storeCategory->description)), 0, ',', '');?>
                                        </strong> <span class = "smallGreen">руб.</span>
                                    </td>
                                    <td>
                                    <td>
                                        <input 
                                            type="checkbox"
                                            checked="checked"
                                            cost="<?php echo floor(($product->price / 100) * strip_tags($storeCategory->description));?>"
                                            etalonCost="<?php echo floor(((($product->price / 100) * strip_tags($storeCategory->description))/60)*$step);?>"
                                            name = "product[<?php echo $spec?>][<?php echo $num;?>]"
                                            value = "<?php echo $product->id;?>"
                                            id="checkbox-id-<?php echo $spec;?>-<?php echo $product->id;?>" />
                                        <label for="checkbox-id-spec3-<?php echo $product->id;?>"></label>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </table>
                    </div>
                    <div class="row specTable spec4">
                        <table>
                            <tr class = "cpecHead">
                                <td colspan="2">Специалисты:</td>
                                <td>Состав работ:</td>
                                <td>Стоимость:</td>
                            </tr>
                            <tr>
                            <?php $spec = 'spec4';?>
                            <?php $storeCategory = StoreCategory::model()->findByAttributes(['slug' => $spec]);?>
                            <?php foreach(Product::getByCategorySlug($spec) as $num => $product):?>
                                <tr>
                                    <td>
                                        <img src = "<?php echo $product->getImageUrl();?>">
                                    </td>
                                    <td class ="rightBorderGray">
                                        <strong class = "specName"><?php echo $product->name;?></strong>
                                        <p class = "grayText"><?php echo $product->description;?></p>
                                    </td>
                                    <td class ="rightBorderGray bigRed">
                                        <p class = "bigRed"><?php echo $product->short_description;?></p>
                                    </td>
                                    <td style="width: 140px;">
                                        <strong class = "bigGreen"><?php echo number_format(floor(($product->price / 100) * strip_tags($storeCategory->description)), 0, ',', '');?>
                                        </strong> <span class = "smallGreen">руб.</span>
                                    </td>
                                    <td>
                                        <input 
                                            type="checkbox"
                                            checked="checked"
                                            cost="<?php echo floor(($product->price / 100) * strip_tags($storeCategory->description));?>"
                                            etalonCost="<?php echo floor(((($product->price / 100) * strip_tags($storeCategory->description))/60)*$step);?>"
                                            name = "product[<?php echo $spec?>][<?php echo $num;?>]"
                                            value = "<?php echo $product->id;?>"
                                            id="checkbox-id-<?php echo $spec;?>-<?php echo $product->id;?>" />
                                        <label for="checkbox-id-<?php echo $spec;?>-<?php echo $product->id;?>"></label>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                            </tr>
                        </table>
                    </div>
                    <div class="row specTable spec5">
                        <table>
                            <tr class = "cpecHead">
                                <td colspan="2">Специалисты:</td>
                                <td>Состав работ:</td>
                                <td>Стоимость:</td>
                            </tr>
                            <?php $spec = 'spec5';?>
                            <?php $storeCategory = StoreCategory::model()->findByAttributes(['slug' => $spec]);?>
                            <?php foreach(Product::getByCategorySlug($spec) as $num => $product):?>
                                <tr>
                                    <td>
                                        <img src = "<?php echo $product->getImageUrl();?>">
                                    </td>
                                    <td class ="rightBorderGray">
                                        <strong class = "specName"><?php echo $product->name;?></strong>
                                        <p class = "grayText"><?php echo $product->description;?></p>
                                    </td>
                                    <td class ="rightBorderGray bigRed">
                                        <p class = "bigRed"><?php echo $product->short_description;?></p>
                                    </td>
                                    <td style="width: 140px;">
                                        <strong class = "bigGreen"><?php echo number_format(floor(($product->price / 100) * strip_tags($storeCategory->description)), 0, ',', '');?>
                                        </strong> <span class = "smallGreen">руб.</span>
                                    </td>
                                    <td>
                                        <input 
                                            type="checkbox"
                                            checked="checked"
                                            cost="<?php echo floor(($product->price / 100) * strip_tags($storeCategory->description));?>"
                                            etalonCost="<?php echo floor(((($product->price / 100) * strip_tags($storeCategory->description))/60)*$step);?>"
                                            name = "product[<?php echo $spec?>][<?php echo $num;?>]"
                                            value = "<?php echo $product->id;?>"
                                            id="checkbox-id-<?php echo $spec;?>-<?php echo $product->id;?>" />
                                        <label for="checkbox-id-<?php echo $spec;?>-<?php echo $product->id;?>"></label>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                        </table>
                    </div>
                    <div class = "row">
                        <div class = "check">
                            <div class = "sumWrap"><strong>0</strong><span>руб.</span></div>
                        </div>
                    </div>
                    <div class = "row centeredText" style = "margin-top:10px;margin-bottom:20px;">
                        <strong style = "font-size: 20px;">Для формирования заявки укажите свою почту или телефон</strong>
                    </div>
                    <div class = "row centeredText">
                        <section class="col-sm-6 centeredText">
                            <input type="text" class="form-control" name= "phone" placeholder="Ваш skype или Сотовый телефон">
                        </section>
                        <section class="col-sm-6 centeredText">
                            <input type="text" class="form-control" name = "email" placeholder="Электронная почта">
                        </section>
                    </div>
                    <div class = "row centeredText " style = "margin-top: 20px;">
                        <input type="hidden" name="curSpec" value="spec3">
                        <input type="hidden" name="totalPrice" value="">
                        <input type="hidden" name="currentProducts" value="">
                        <input type="hidden" name="prices" value="">
                        <input style = "padding-top: 10px; padding-bottom: 10px; font-size: 18px;" type="submit" name="" value = "Заказать выбранный ролик" class = "orderVIdeoSubmit btn btn-default">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

$('#calcModal').on('click', 'label', function(){

    if ($(this).parent().parent().find('.bigGreen').hasClass('bigGrey')){
        $(this).parent().parent().find('.bigGreen').removeClass('bigGrey');
        $(this).parent().parent().find('.smallGreen').removeClass('bigGrey');
    }else{
        $(this).parent().parent().find('.bigGreen').addClass('bigGrey');;
        $(this).parent().parent().find('.smallGreen').addClass('bigGrey');;
    }
    return true;
})

var stap = <?php echo $step;?>;
$(document).on("click", '.plusBtn', function(){

    var minute = parseInt($('#minute').val());
    var second = parseInt($('#second').val());
    if (second < 60){
        second += stap;
    }else{
        minute++;
        second = stap;
    }
    $('#second').val(second);
    $('#minute').val(minute);

    calculateOrderSum();

})
$(document).on("click", '.minusBtn', function(){

    var minute = parseInt($('#minute').val());
    var second = parseInt($('#second').val());
    if (second > 0){
        if (minute > 0){
            second -= stap;
        }
    }else{
        if (minute > 0){
            minute--;
            second = 60;
        }
    }
    $('#second').val(second);
    $('#minute').val(minute);

    calculateOrderSum();

})

$('.levelSlozh section').click(function(){
    $('.specTable').hide();
    $('.specTable').removeClass('currentSpec');
    $('.levelSlozh').find('.currentWr').removeClass('currentWr');

    $('.' + $(this).attr('showSpesc')).show();
    $('.' + $(this).attr('showSpesc')).addClass('currentSpec');
    $(this).addClass('currentWr');

    $('[name="curSpec"]').val($(this).attr('showSpesc'));

    calculateOrderSum();
})

$('#calcModal').on('change', '[type=checkbox]', function(){
    calculateOrderSum();
})

function calculateOrderSum(){
    var resultSum = 0;
    var minute = parseInt($('#minute').val());
    var second = parseInt($('#second').val());

    var resultSeconds = 60 * minute + second;

    $('#calcModal').find('[type=checkbox]').each(function( index ) {
        var ckeckBoxCost = parseInt($(this).attr('cost'));
        var ckeckBoxEtalon = parseInt($(this).attr('etalonCost'));
        resCost = (resultSeconds / stap) * ckeckBoxEtalon;
        $(this).attr('cost', resCost);

        $(this).parent().parent().find('.bigGreen').html(resCost);
    });

    var prices = '';
    var currentProducts = '';
    $('.currentSpec').find('[type=checkbox]').each(function( index ) {

        if ($(this).is(':checked')){
            resultSum += parseInt($(this).attr('cost'));
            prices = prices + $(this).attr('cost') + ',';
            currentProducts = currentProducts + $(this).val() + ',';
        }
    });
    $('.sumWrap').find('strong').html(resultSum);
    $('[name="totalPrice"').val(resultSum);
    $('[name="currentProducts"').val(currentProducts);
    $('[name="prices"').val(prices);
}

$('#calcModal').on('click', '.orderVIdeoSubmit', function(){

    $('#calcModal').find('.error').remove();

    if ($('#calcModal').find('[name="phone"]').val()== ''){
        $('#calcModal').find('[name="phone"]').parent().append('<i class = "error">Необходимо заполнить</i>');
        return false;
    }

    if ($('#calcModal').find('[name="email"]').val() == ''){
        $('#calcModal').find('[name="email"]').parent().append('<i class = "error">Необходимо заполнить</i>');
        return false;
    }
    return true;
})

$(function() {
    calculateOrderSum();
});

$(document).on("click", ".wantItButton",  function(){
    $('#orderVideoModal').modal();
})
</script>