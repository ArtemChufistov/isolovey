<div class="container headButtons">
    <div class="row">
        <button type="button" class="btn btn-default" aria-label="Left Align" id="orderVideo" data-toggle="modal" data-target="#orderVideoModal">Заказать ролик!</button>
        <button type="button" class="btn btn-default" aria-label="Left Align" id="calcVideo" data-toggle="modal" data-target="#calcModal">
            Калькулятор стоимости <i class="fa fa-calculator" aria-hidden="true"></i>
        </button>
        <strong>mail@isolovey.ru</strong>
        <strong class = "verticalLine">|</strong>
        <span>+7 (383)</span>
        <strong>383-07-16</strong>
    </div>
</div>
<div class="line"></div>

<div id="calcModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><button class="close" type="button" data-dismiss="modal">×</button>
            <h4 class="modal-title">Калькулятор стоимости</h4>
            </div>
            <div class="modal-body">Длительность ролика</div>
            <div class="modal-footer"><button class="btn btn-default" type="button" data-dismiss="modal">Заказать выбранный ролик</button></div>
        </div>
    </div>
</div>

<div id="orderVideoModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">Заказать видеоролик</div>
            <div class="modal-footer"><button class="btn btn-default" type="button" data-dismiss="modal">Заказать ролик</button></div>
        </div>
    </div>
</div>