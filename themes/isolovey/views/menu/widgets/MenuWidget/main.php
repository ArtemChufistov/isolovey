<nav class="navbar navbar-fixed-top" id="sidebar-wrapper" role="navigation">
    <p class="sidebar-brand">
        <a href="/">
           <img src = "/img/solovey.png">
        </a>
        Студия &laquo;Соловей&raquo; </br>
        - создания понятных</br> видеороликов
    </p>
    <ul class="nav sidebar-nav">
        <?php foreach($this->params['items'] as $item):?>
        <?php $url = is_array($item['url']) ? $item['url'][0] : $item['url'];?>
        <li class="<?php if (!empty($item['items'])):?>dropdown open"<?php endif;?> <?php if ($url == Yii::app()->request->requestUri):?>current<?php endif;?>">
            <a href="<?php echo $url;?>" <?php if (!empty($item['items'])):?>class="dropdown-toggle" data-toggle="dropdown"<?php endif;?> >
                <?php echo $item['label'];?> <?php if (!empty($item['items'])):?> <span class="caret"></span><?php endif;?>
            </a>
            <?php if (!empty($item['items'])):?>
              <ul class="dropdown-menu" role="menu">
                <?php foreach($item['items'] as $subitem):?>
                  <?php $url = is_array($subitem['url']) ? $subitem['url'][0] : $subitem['url'];?>
                  <li class ="<?php if ($url == Yii::app()->request->requestUri):?>current<?php endif;?>" ><a href="<?php echo $url;?>"><?php echo $subitem['label'];?></a></li>
                <?php endforeach;?>
              </ul>
            <?php endif;?>
        </li>
        <?php endforeach;?>
    </ul>
</nav>

<script>
$(document).ready(function () {
  var trigger = $('.hamburger'),
      overlay = $('.overlay'),
     isClosed = false;

    trigger.click(function () {
      hamburger_cross();      
    });

    function hamburger_cross() {

      if (isClosed == true) {          
        overlay.hide();
        trigger.removeClass('is-open');
        trigger.addClass('is-closed');
        isClosed = false;
      } else {   
        overlay.show();
        trigger.removeClass('is-closed');
        trigger.addClass('is-open');
        isClosed = true;
      }
  }
  
  $('[data-toggle="offcanvas"]').click(function () {
        $('#wrapper').toggleClass('toggled');
  });  
});
</script>