<div class="col-item projectWrap">
    <div class="photo">
        <div class = "project">
            <strong><?php echo $data->name;?></strong></br></br>
            <span><?php echo strip_tags($data->description);?></span></br>
            <span class = "podrobnee">Подробнее...</span>
        </div>
        <img src="<?= StoreImage::product($data, 190, 190, false); ?>"
             alt="<?= CHtml::encode($data->getImageAlt()); ?>"
             title="<?= CHtml::encode($data->getImageTitle()); ?>"
        />
    </div>
    <div class="modal fade videoModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"  <?php if (!empty($data->description )):?>style = "top: -110px;"<?php endif;?>>
                    <button class="close" type="button" data-dismiss="modal" <?php if (!empty($data->description )):?>style = "margin-top: 90px;"<?php endif;?>></button>
                    <i><?php echo strip_tags($data->description);?></i></br>
                    <h4 class="modal-title"><?php echo $data->name;?></h4>
                    <?php if (!empty($data->short_description )):?>
                            <?php echo $data->short_description; ?>
                    <?php endif;?>
                </div>
                <div class="modal-body"  >
                    <?php echo $data->data; ?>
                </div>
                <div class="modal-footer centeredText"><button class="btn btn-default wantItButton" type="button" data-dismiss="modal">Хочу такой-же ролик!</button></div>
            </div>
        </div>
    </div>
</div>