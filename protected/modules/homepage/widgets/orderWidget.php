<?php
 Yii::import('application.modules.store.models.Product');
 Yii::import('application.modules.order.models.OrderProduct');
 Yii::import('application.modules.order.models.Order');

class OrderWidget extends yupe\widgets\YWidget
{
    /**
     * Запускаем отрисовку виджета
     *
     * @return void
     */
    public function run()
    {
    	$order = new Order;
    	if (!empty($_POST['phone'])){

    		$order = new Order;

            if (isset($_POST['minute']) && isset($_POST['second'])){

                $productIds = explode(',', $_POST['currentProducts']);
                $costs      = explode(',', $_POST['prices']);

                $products = [];
                foreach($productIds as $productId){
                    if ($productId != ''){
                        $products[] = Product::model()->findByPk($productId);
                    }
                }


                $order->phone = $_POST['phone'];
                $order->email = $_POST['email'];
                $order->total_price = $_POST['totalPrice'];
                $order->status_id = OrderStatus::STATUS_NEW; 
                $order->note = 'Длительность: ' . $_POST['minute'] . ' минут' . $_POST['second'] . ' секунд';

                $order->save(false);

                foreach ($products as $key => $product) {
                    $orderProduct = new OrderProduct;
                    $orderProduct->price = $costs[$key];
                    $orderProduct->quantity = 1;
                    $orderProduct->order_id = $order->id;
                    $orderProduct->product_id = $product->id;
                    $orderProduct->product_name = $product->name;

                    $orderProduct->save(false);
                }
                $order->save(false);

                $this->sendSms($order);

            }else{
                $order->phone = $_POST['phone'];
                $order->save(false);

                $this->sendSms($order);
            }
    	}
        $this->render('orderView', array('order' => $order));
    }

    public function sendSms($order)
    {
         if( $curl = curl_init() ) {

            $login = 'solje';
            $password = 'trampampam123';
            $phones = '+79537778897';
            $mes = 'U vas na saite noviy zakaz ' . $order->id;

            if ($order->total_price != 0){
                $mes .= ' na summu ' . $order->total_price;
            }

            $mes .= ' kontakti ' . $order->phone;

            curl_setopt($curl, CURLOPT_URL, 'http://smsc.ru/sys/send.php?login=' . $login. '&psw=' . $password . '&phones=' . $phones . '&mes=' . $mes);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            $out = curl_exec($curl);
            curl_close($curl);
          }
    }
}
